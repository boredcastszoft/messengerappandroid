package com.boredcast.messengerapp.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CalendarView;

import com.boredcast.log.CustomLogger;
import com.boredcast.log.Logging;
import com.boredcast.messengerapp.R;

public class MeetingOrganiseActivity extends AppCompatActivity {

    private static final CustomLogger log = Logging.getLogger(MeetingOrganiseActivity.class);

    CalendarView calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_organise);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        calendar = findViewById(R.id.calendarView);
        calendar.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            log.info("asd: " + year + ":" + month + ":" + dayOfMonth);
        });
    }
}
