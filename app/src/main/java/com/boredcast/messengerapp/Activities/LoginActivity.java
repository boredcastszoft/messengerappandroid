package com.boredcast.messengerapp.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.boredcast.log.CustomLogger;
import com.boredcast.log.Logging;
import com.boredcast.messengerapp.Dialogs.RegisterDialog;
import com.boredcast.messengerapp.MessengerApplication;
import com.boredcast.messengerapp.R;
import com.boredcast.messengerapp.Tasks.BasicCommTask;
import com.boredcast.utils.ResponseCallback;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements ResponseCallback {

    private static final CustomLogger log = Logging.getLogger(LoginActivity.class);

    private class LoginResponse {
        private static final String OK = "ok";
        private static final String NOT_OK = "nop";
        private static final String WRONG = "wus";
    }

    private EditText nameField;
    private EditText passwordField;
    private ProgressBar progressBar;
    private BasicCommTask loginTask;
    private int backPressed = 0;

    private RegisterDialog registerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        getSupportActionBar().hide();
        Button loginButton = findViewById(R.id.LoginButton);
        final Button registerButton = findViewById(R.id.RegisterButton);
        nameField = findViewById(R.id.NameField);
        passwordField = findViewById(R.id.PasswordField);
        progressBar = findViewById(R.id.progressbar_login);

        loginButton.setOnClickListener(view -> {
            progressBar.setVisibility(View.VISIBLE);
            String name = nameField.getText().toString();
            String pass = passwordField.getText().toString();
            log.info("name: " + name + " pass: " + pass);
            if (loginTask != null) {
                loginTask.cancel(true);
            }
            if (!name.equals("") || !(pass.equals(""))) {
                JSONObject json = new JSONObject();

                try {
                    json.put("userName", name);
                    json.put("password", pass);
                } catch (JSONException e) {
                    log.error(e);
                }

                loginTask = new BasicCommTask("login", json, LoginActivity.this, LoginActivity.this);
                loginTask.execute();
            }
        });

        registerButton.setOnClickListener(v -> {
            registerDialog = new RegisterDialog(LoginActivity.this, LoginActivity.this);
            registerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            registerDialog.setCanceledOnTouchOutside(false);
            registerDialog.show();
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (loginTask != null) {
            loginTask.cancel(true);
        }
        if (registerDialog != null) {
            registerDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        backPressed++;
        if(backPressed >= 2) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                this.finishAndRemoveTask();
            } else {
                this.finishAffinity();
            }
        } else {
            Toast.makeText(this, "Press back again to leave!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResponse(String kind, JSONObject json) {

        if (progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }

        if (kind != null) {
            switch (kind) {
                case "login":
                    try {
                        String state = json.getString("state");
                        switch (state) {
                            case LoginResponse.OK:
                                String id = json.getString("id");
                                MessengerApplication.config.edit().putString(MessengerApplication.PreferenceKeys.USER_ID, id).apply();
                                Intent intent = new Intent(this, MessageSenderActivity.class);
                                startActivity(intent);
                                break;
                            case LoginResponse.NOT_OK:
                                Toast.makeText(this, "There is no user with this name!", Toast.LENGTH_SHORT).show();
                                break;
                            case LoginResponse.WRONG:
                                Toast.makeText(this, "Wrong Username or Password!", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    } catch (JSONException e) {
                        progressBar.setVisibility(View.GONE);
                    }
                    break;
                case "register":
                    try {
                        String state = json.getString("state");
                        switch (state) {
                            case MessengerApplication.Response.OK:
                                Toast.makeText(this, "Successfully registered!", Toast.LENGTH_SHORT).show();
                                registerDialog.dismiss();
                                break;
                            case MessengerApplication.Response.NOT_OK:
                                Toast.makeText(this, "There is already a User with this name!", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    } catch (JSONException e) {
                        log.error(e);
                    }
                    break;

            }
        }
    }
}
