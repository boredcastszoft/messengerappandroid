package com.boredcast.messengerapp.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.boredcast.log.CustomLogger;
import com.boredcast.log.Logging;
import com.boredcast.messengerapp.Dialogs.CreateGroupDialog;
import com.boredcast.messengerapp.Dialogs.FriendRequestDialog;
import com.boredcast.messengerapp.Dialogs.FriendRequestsDialog;
import com.boredcast.messengerapp.Dialogs.LogoutDialog;
import com.boredcast.messengerapp.Friend;
import com.boredcast.messengerapp.Group;
import com.boredcast.messengerapp.Message;
import com.boredcast.messengerapp.MessengerApplication;
import com.boredcast.messengerapp.PollingService;
import com.boredcast.messengerapp.R;
import com.boredcast.messengerapp.Tasks.BasicCommTask;
import com.boredcast.messengerapp.Tasks.GetDataTask;
import com.boredcast.messengerapp.Tasks.SendFilesToServer;
import com.boredcast.messengerapp.Tasks.SendMessagesToServer;
import com.boredcast.messengerapp.Views.FriendView;
import com.boredcast.messengerapp.Views.MessageField;
import com.boredcast.messengerapp.Views.MessageView;
import com.boredcast.utils.ResponseCallback;
import com.boredcast.utils.ToActivitiyCallback;
import com.boredcast.utils.Tools;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MessageSenderActivity extends AppCompatActivity implements ResponseCallback, ToActivitiyCallback {

    private static final CustomLogger log = Logging.getLogger(MessageSenderActivity.class);

    private static final int PICK_FILE_RESULT_CODE = 1;

    private MessageField messageField;
    private Button sendButton;
    private Button interractionsButton;
    private LinearLayout friendsLayout;
    private ScrollView scrollView;
    private LinearLayout messagesLayout;
    private ProgressBar progressBar;
    private FriendRequestDialog friendRequestDialog;
    private CreateGroupDialog createGroupDialog;
    private LogoutDialog logoutDialog;
    private FriendRequestsDialog friendRequestsDialog;

    private String messageTo;

    private GetDataTask getFriendsFromServerTask;
    private GetDataTask getGroupsFromServerTask;
    private GetDataTask getMessagesFromServerTask;
    private BasicCommTask deleteGroupTask;
    private SendMessagesToServer sendMessageToServerTask;
    private SendFilesToServer sendFilesToServerTask;

    private IntentFilter serverFilter = new IntentFilter();

    private BroadcastReceiver serverReceiver;

    private Map<Integer, String> data;
    private List<Friend> friends;
    private List<Group> groups;
    private List<FriendView> friendViews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_sender);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        messageField = findViewById(R.id.message_field);
        scrollView = findViewById(R.id.message_holder);
        messagesLayout = findViewById(R.id.message_layout);
        sendButton = findViewById(R.id.send_button);
        interractionsButton = findViewById(R.id.interractions_button);
        friendsLayout = findViewById(R.id.friend_layout);
        progressBar = findViewById(R.id.progressbar_sender);

        serverReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final String action = intent.getAction();
                if (action != null) {
                    switch (action) {
                        case MessengerApplication.ServerActions.MESSAGE:
                            log.info("receive: " + intent.getStringExtra("json"));
                            try {
                                JSONObject json = new JSONObject(intent.getStringExtra("json"));
                                JSONArray arr = json.getJSONArray("data");
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject jsonOb = arr.getJSONObject(i);
                                    String sender = jsonOb.getString("sender");
                                    String receiver = jsonOb.getString("receiver");
                                    String message = jsonOb.getString("message");
                                    if (!message.equals("Fail")) {
                                        long sentTime = Long.valueOf(jsonOb.getString("sentTime"));
                                        if (receiver.equals(MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null)) &&
                                                sender.equals(messageTo)) {
                                            MessageView messageView = new MessageView(MessageSenderActivity.this);
                                            messageView.setMessage(null, sender, message, Tools.getDateFromTimeStamp(sentTime));
                                            messagesLayout.addView(messageView);
                                            scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                log.error(e);
                            }
                            break;
                    }
                }
            }
        };
        serverFilter.addAction(MessengerApplication.ServerActions.MESSAGE);
        initialize();

    }

    @Override
    protected void onResume() {
        log.info("onResume-start");
        LocalBroadcastManager.getInstance(this).registerReceiver(serverReceiver, serverFilter);
        super.onResume();
        log.info("onResume-end");
    }

    protected void onPause() {
        log.info("onPause-start");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(serverReceiver);
        super.onPause();
        log.info("onPause-end");
    }

    protected void onStop() {
        super.onStop();
        log.info("onStop-start");
        log.info("onStop-end");
    }

    @Override
    protected void onDestroy() {
        log.info("onDestroy-start");
        super.onDestroy();
        friendsLayout.removeAllViews();
        messagesLayout.removeAllViews();
        stopService(new Intent(this, PollingService.class));
        if (getFriendsFromServerTask != null) {
            getFriendsFromServerTask.cancel(true);
        }
        if (sendMessageToServerTask != null) {
            sendMessageToServerTask.cancel(true);
        }
        if (getMessagesFromServerTask != null) {
            getMessagesFromServerTask.cancel(true);
        }
        if (sendFilesToServerTask != null) {
            sendFilesToServerTask.cancel(true);
        }
        if (createGroupDialog != null) {
            createGroupDialog.dismiss();
        }
        if (logoutDialog != null) {
            logoutDialog.dismiss();
        }
        if (friendRequestDialog != null) {
            friendRequestDialog.dismiss();
        }
        if (friendRequestsDialog != null) {
            friendRequestsDialog.dismiss();
        }
        log.info("onDestroy-end");
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(v.getId(), 0, Menu.NONE, "Create Group");
        menu.add(v.getId(), 1, Menu.NONE, "Send Friend request");
        menu.add(v.getId(), 2, Menu.NONE, "My Friend requests");
        if (friendViews != null) {
            for (FriendView fw : friendViews) {
                if (fw.isHighlighted()) {
                    for (Group g : groups) {
                        if (fw.getFriend().equals(g.getName())) {
                            //menu.add(v.getId(), 3, Menu.NONE, "Organise Meeting");
                            menu.add(v.getId(), 3, Menu.NONE, "Delete group");
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                createGroupDialog = new CreateGroupDialog(this, this);
                createGroupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                createGroupDialog.setCanceledOnTouchOutside(false);
                createGroupDialog.show();
                break;
            case 1:
                friendRequestDialog = new FriendRequestDialog(this, this);
                friendRequestDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                friendRequestDialog.setCanceledOnTouchOutside(false);
                friendRequestDialog.show();
                break;
            case 2:
                friendRequestsDialog = new FriendRequestsDialog(this, this, this);
                friendRequestsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                friendRequestsDialog.setCanceledOnTouchOutside(false);
                friendRequestsDialog.setCancelable(false);
                friendRequestsDialog.show();
                break;
            case 3:
                /*Intent intent = new Intent(this, MeetingOrganiseActivity.class); // TODO: 2018. 11. 14. group adatait átadni
                startActivity(intent);*/
                if (deleteGroupTask != null) {
                    deleteGroupTask.cancel(true);
                }
                String groupName = null;
                if (friendViews != null) {
                    for (FriendView fw : friendViews) {
                        if (fw.isHighlighted()) {
                            for (Group g : groups) {
                                if (fw.getFriend().equals(g.getName())) {
                                    groupName = g.getName();
                                }
                            }
                        }
                    }
                }
                if (groupName != null) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("groupName", groupName);
                    } catch (JSONException ignored) {
                    }
                    JSONObject plus = new JSONObject();
                    try {
                        plus.put("name", groupName);
                    } catch (JSONException ignored) {
                    }
                    deleteGroupTask = new BasicCommTask("deletegroup", jsonObject, this, this, plus);
                    deleteGroupTask.execute();
                }
                break;
        }
        return true;
    }

    private void initialize() {

        sendButton.setOnClickListener(view -> {
            if (messageField != null) {
                String message = messageField.getText().toString();
                String name = MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null);
                String timestamp = Tools.getDateFromTimeStamp(System.currentTimeMillis());

                if (!message.equals("")) {

                    MessageView messageView = new MessageView(MessageSenderActivity.this);
                    messageView.setMessage(null, name, message, timestamp);
                    messagesLayout.addView(messageView);
                    scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));

                    messageField.getText().clear();

                    if (!messageField.hasFile()) {
                        JSONObject json = new JSONObject();
                        try {
                            json.put("sender", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));
                            json.put("receiver", messageTo);
                            json.put("message", message);
                            json.put("sentTime", System.currentTimeMillis());
                        } catch (JSONException e) {
                            log.error(e);
                        }

                        if (sendMessageToServerTask != null) {
                            sendMessageToServerTask.cancel(true);
                        }
                        sendMessageToServerTask = new SendMessagesToServer(json, MessageSenderActivity.this);
                        sendMessageToServerTask.execute();
                    } else {
                        File file = messageField.getFile();
                        messageField.setHasFile(false);
                        if (sendFilesToServerTask != null) {
                            sendFilesToServerTask.cancel(true);
                        }
                        sendFilesToServerTask = new SendFilesToServer(file, MessageSenderActivity.this);
                        sendFilesToServerTask.execute();
                    }
                }
            }
        });

        registerForContextMenu(interractionsButton);

        JSONObject json = new JSONObject();
        try {
            json.put("userName", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));
        } catch (JSONException e) {
            log.error(e);
        }

        if (getFriendsFromServerTask != null) {
            getFriendsFromServerTask.cancel(true);
        }

        progressBar.setVisibility(View.VISIBLE);
        getFriendsFromServerTask = new GetDataTask("friends", json, MessageSenderActivity.this, MessageSenderActivity.this);
        getFriendsFromServerTask.execute();

        if (getGroupsFromServerTask != null) {
            getGroupsFromServerTask.cancel(true);
        }

        getGroupsFromServerTask = new GetDataTask("getgroups", json, MessageSenderActivity.this, MessageSenderActivity.this);
        getGroupsFromServerTask.execute();

        Intent intent = new Intent(this, PollingService.class);
        startService(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PICK_FILE_RESULT_CODE) {

            if (resultCode == RESULT_OK) {
                try {
                    Uri uri = data.getData();

                    String filename;
                    String mimeType = getContentResolver().getType(uri);
                    if (mimeType == null) {
                        String path = Tools.getPath(this, uri);
                        if (path == null) {
                            filename = FilenameUtils.getName(uri.toString());
                        } else {
                            File file = new File(path);
                            filename = file.getName();
                        }
                    } else {
                        Uri returnUri = data.getData();
                        Cursor returnCursor = getContentResolver().query(returnUri, null, null, null, null);
                        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                        returnCursor.moveToFirst();
                        filename = returnCursor.getString(nameIndex);
                        String size = Long.toString(returnCursor.getLong(sizeIndex));
                    }

                    File chosenFile = new File(filename);
                    messageField.setText("Chosen File: " + chosenFile.getName());
                    messageField.setFile(chosenFile);

                } catch (Exception e) {
                    log.error(e);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        logoutDialog = new LogoutDialog(this, this);
        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        logoutDialog.setCanceledOnTouchOutside(false);
        logoutDialog.show();
    }

    @Override
    public void onResponse(String kind, JSONObject json) {

        if (progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }

        switch (kind) {
            case "friends":
                friendViews = new ArrayList<>();
                friends = new ArrayList<>();
                data = new TreeMap<>();
                try {
                    JSONArray arr = json.getJSONArray("data");
                    for (int i = 0; i < arr.length(); i++) {
                        log.info("asd" + i);
                        JSONObject tmp = arr.getJSONObject(i);
                        String name = tmp.getString("userFriendName");
                        data.put((i + 1), name);
                        friends.add(new Friend(name));

                    }
                } catch (JSONException e) {
                    log.error(e);
                }

                for (Map.Entry<Integer, String> entries : data.entrySet()) {
                    FriendView tw = new FriendView(this);
                    tw.setFriend(entries.getValue(), null);
                    tw.setId(entries.getKey());
                    tw.setOnClickListener(view -> {
                        for (FriendView fw : friendViews) {
                            if (fw.isHighlighted()) {
                                fw.highlight(false);
                            } else if (fw.getId() == view.getId() && !fw.isHighlighted()) {
                                fw.highlight(true);
                                log.info("friend " + fw.getId());
                            }
                        }

                        messageTo = data.get(view.getId());

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("sender", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));
                            jsonObject.put("receiver", messageTo);
                        } catch (JSONException e) {
                            log.error(e);
                        }

                        if (getMessagesFromServerTask != null) {
                            getMessagesFromServerTask.cancel(true);
                        }

                        getMessagesFromServerTask = new GetDataTask("getmessages", jsonObject, MessageSenderActivity.this, MessageSenderActivity.this);
                        getMessagesFromServerTask.execute();
                    });
                    friendsLayout.addView(tw);
                    friendViews.add(tw);
                    messageTo = friendViews.get(0).getFriend();
                    friendViews.get(0).highlight(true);
                }

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("sender", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));
                    jsonObject.put("receiver", messageTo);
                } catch (JSONException e) {
                    log.error(e);
                }

                if (getMessagesFromServerTask != null) {
                    getMessagesFromServerTask.cancel(true);
                }

                getMessagesFromServerTask = new GetDataTask("getmessages", jsonObject, MessageSenderActivity.this, MessageSenderActivity.this);
                getMessagesFromServerTask.execute();
                break;
            case "getgroups":
                int initSize = data.size();
                groups = new ArrayList<>();
                try {
                    JSONArray arr = json.getJSONArray("data");
                    int j = 0;
                    for (int i = initSize; i < arr.length() + initSize; i++) {
                        JSONObject tmp = arr.getJSONObject(j);
                        String name = tmp.getString("groupName");
                        data.put((i + 1), name);
                        groups.add(new Group(name));

                        j++;
                    }
                } catch (JSONException e) {
                    log.error(e);
                }

                for (Map.Entry<Integer, String> entries : data.entrySet()) {
                    log.info(entries.getKey() + ":" + entries.getValue());
                    if (entries.getKey() > initSize) {
                        FriendView tw = new FriendView(this);
                        tw.setFriend(entries.getValue(), null);
                        tw.setId(entries.getKey());
                        tw.setOnClickListener(view -> {
                            for (FriendView fw : friendViews) {
                                if (fw.isHighlighted()) {
                                    fw.highlight(false);
                                } else if (fw.getId() == view.getId() && !fw.isHighlighted()) {
                                    fw.highlight(true);
                                    log.info("group " + fw.getId());
                                }
                            }

                            messageTo = data.get(view.getId());

                            log.info("mes: " + messageTo);

                            JSONObject jsona = new JSONObject();
                            try {
                                jsona.put("sender", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));
                                jsona.put("receiver", messageTo);
                            } catch (JSONException e) {
                                log.error(e);
                            }

                            if (getMessagesFromServerTask != null) {
                                getMessagesFromServerTask.cancel(true);
                            }

                            getMessagesFromServerTask = new GetDataTask("getmessages", jsona, MessageSenderActivity.this, MessageSenderActivity.this);
                            getMessagesFromServerTask.execute();
                        });
                        friendsLayout.addView(tw);
                        friendViews.add(tw);
                        messageTo = friendViews.get(0).getFriend();
                        friendViews.get(0).highlight(true);
                    }
                }

                JSONObject jsonaa = new JSONObject();
                try {
                    jsonaa.put("sender", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));
                    jsonaa.put("receiver", messageTo);
                } catch (JSONException e) {
                    log.error(e);
                }

                if (getMessagesFromServerTask != null) {
                    getMessagesFromServerTask.cancel(true);
                }

                getMessagesFromServerTask = new GetDataTask("getmessages", jsonaa, MessageSenderActivity.this, MessageSenderActivity.this);
                getMessagesFromServerTask.execute();
                break;
            case "getmessages":
                try {
                    messagesLayout.removeAllViews();
                    List<Message> receivedMessages = new ArrayList<>();

                    JSONArray arr = json.getJSONArray("data");
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject jsonOb = arr.getJSONObject(i);
                        log.info(jsonOb.toString());
                        String sender = jsonOb.getString("sender");
                        String receiver = jsonOb.getString("receiver");
                        String message = jsonOb.getString("message");
                        long sentTime = Long.valueOf(jsonOb.getString("sentTime"));
                        receivedMessages.add(new Message(sender, receiver, message, sentTime));
                    }

                    Collections.sort(receivedMessages, (o1, o2) -> {
                        if (o1.getSentTime() > o2.getSentTime()) {
                            return 1;
                        } else if (o1.getSentTime() == o2.getSentTime()) {
                            return 0;
                        }
                        return -1;
                    });

                    for (Message m : receivedMessages) {
                        MessageView mw = new MessageView(this);
                        mw.setMessage(null, m.getSender(), m.getMessage(), Tools.getDateFromTimeStamp(m.getSentTime()));
                        messagesLayout.addView(mw);
                    }

                } catch (JSONException e) {
                    log.error(e);
                }
                scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));
                break;
            case "friendrequest":
                try {
                    String state = json.getString("state");
                    if (state.equals(MessengerApplication.Response.OK)) {
                        Toast.makeText(this, "Friendrequest sent!", Toast.LENGTH_SHORT).show();
                        friendRequestDialog.dismiss();
                    }
                } catch (JSONException e) {
                    log.error(e);
                }
                break;
            case "creategroup":
                try {
                    String state = json.getString("state");
                    if (state.equals(MessengerApplication.Response.OK)) {
                        String groupName = json.getString("groupName");
                        data.put(data.size() + 1, groupName);
                        FriendView tw = new FriendView(this);
                        tw.setFriend(groupName, null);
                        tw.setOnClickListener(view -> {
                            for (FriendView fw : friendViews) {
                                if (fw.isHighlighted()) {
                                    fw.highlight(false);
                                } else if (fw.getId() == view.getId() && !fw.isHighlighted()) {
                                    fw.highlight(true);
                                }
                            }

                            messageTo = data.get(view.getId());

                            JSONObject jsonObjecta = new JSONObject();
                            try {
                                jsonObjecta.put("sender", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));
                                jsonObjecta.put("receiver", messageTo);
                            } catch (JSONException e) {
                                log.error(e);
                            }
                            if (getMessagesFromServerTask != null) {
                                getFriendsFromServerTask.cancel(true);
                            }
                            getMessagesFromServerTask = new GetDataTask("getmessages", jsonObjecta, MessageSenderActivity.this, MessageSenderActivity.this);
                            getMessagesFromServerTask.execute();
                        });
                        friendsLayout.addView(tw);
                        friendViews.add(tw);
                    }
                } catch (JSONException e) {
                    log.error(e);
                }
                createGroupDialog.dismiss();
                break;
            case "deletegroup":
                try {
                    String state = json.getString("state");
                    if (state != null) {
                        if (state.equals(MessengerApplication.Response.OK)) {
                            String name = json.getString("plus");
                            for (FriendView fw : friendViews) {
                                if(fw.getFriend().equals(name)) {
                                    friendViews.remove(fw);
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    log.error(e);
                }
                break;
            case "logout":
                try {
                    String state = json.getString("state");
                    if (state != null) {
                        if (state.equals(MessengerApplication.Response.OK)) {
                            Intent intent = new Intent(this, LoginActivity.class);
                            startActivity(intent);
                        }
                    }
                } catch (JSONException e) {
                    log.error(e);
                }
                break;
        }
    }

    @Override
    public void onCall(String kind) {
        if (kind != null) {
            switch (kind) {
                case "friendrequest":
                    log.info("refresh");
                    friendsLayout.removeAllViews();
                    JSONObject json = new JSONObject();
                    try {
                        json.put("userName", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));
                    } catch (JSONException e) {
                        log.error(e);
                    }

                    if (getFriendsFromServerTask != null) {
                        getFriendsFromServerTask.cancel(true);
                    }

                    getFriendsFromServerTask = new GetDataTask("data", json, MessageSenderActivity.this, MessageSenderActivity.this);
                    getFriendsFromServerTask.execute();

                    if (getGroupsFromServerTask != null) {
                        getGroupsFromServerTask.cancel(true);
                    }

                    getGroupsFromServerTask = new GetDataTask("getgroups", json, MessageSenderActivity.this, MessageSenderActivity.this);
                    getGroupsFromServerTask.execute();
                    break;
            }
        }
    }
}
