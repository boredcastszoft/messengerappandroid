package com.boredcast.messengerapp;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.boredcast.log.CustomLogger;
import com.boredcast.log.Logging;
import com.boredcast.utils.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class PollingService extends Service {

    private static final CustomLogger log = Logging.getLogger(PollingService.class);

    private HandlerThread handlerThread;
    private Handler handler;
    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private LocalBroadcastManager localBroadcastManager;

    @Override
    public void onCreate() {
        log.info("onCreate-Begin");
        super.onCreate();
        handlerThread = new HandlerThread("PollingService HandlerThread");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        log.info("onCreate-End");
    }

    @Override
    public void onDestroy() {
        log.info("onDestroy-Begin");
        handlerThread.quit();
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(6000, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
                if (!executorService.awaitTermination(6000, TimeUnit.SECONDS)) {
                    log.warn("polling did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
        super.onDestroy();
        log.info("onDestroy-End");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler.post(new PollingRunnable());
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class PollingRunnable implements Runnable {

        private boolean err = false;

        @Override
        public void run() {
            log.info("Polling-start");
            executorService.scheduleAtFixedRate(() -> {
                JSONObject res = null;
                JSONObject json = new JSONObject();
                try {
                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                    OkHttpClient client = new OkHttpClient();

                    json.put("receiver", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    okhttp3.Request request = new okhttp3.Request.Builder()
                            .url(MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.SERVER_URL, null) + "/polling")
                            .post(body)
                            .build();

                    okhttp3.Response response = client.newCall(request).execute();

                    String networkResp = response.body().string();
                    if (!networkResp.isEmpty()) {
                        log.info(networkResp);
                        networkResp = "{ \"data\" : " + networkResp + "}";
                        res = Tools.parseJSONStringToJSONObject(networkResp);
                        try {
                            int error = res.getInt("status");
                            if (error == 500) {
                                err = true;
                            }
                        } catch (JSONException ignored) {
                        }

                    }
                } catch (Exception ex) {
                    log.error(String.format("{\"result\":\"false\",\"error\":\"%s\"}", ex.getMessage()));
                    err = true;
                }
                if (res != null && !err) {
                    localBroadcastManager.sendBroadcast(new Intent().setAction(MessengerApplication.ServerActions.MESSAGE).putExtra("json", res.toString()));
                } else {
                    log.info("no poll");
                }
            }, 0, 5000, TimeUnit.MILLISECONDS);
            log.info("Polling-end");

        }
    }
}
