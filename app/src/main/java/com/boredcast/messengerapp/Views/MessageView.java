package com.boredcast.messengerapp.Views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.boredcast.messengerapp.R;

public class MessageView extends ConstraintLayout {

    private ImageView messagePicture;
    private TextView messageName;
    private TextView message;
    private TextView messageTime;

    public MessageView(Context context) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.message_layout, this);
        init();
    }

    public MessageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.message_layout, this);
        init();
    }

    public MessageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.message_layout, this);
        init();
    }

    private void init() {
        messagePicture = findViewById(R.id.message_picture);
        messageName = findViewById(R.id.message_name);
        message = findViewById(R.id.message_message);
        messageTime = findViewById(R.id.message_time);
    }

    public ImageView getMessagePicture() {
        return messagePicture;
    }

    public TextView getMessageName() {
        return messageName;
    }

    public TextView getMessage() {
        return message;
    }

    public void setMessage(Drawable drawable, String name, String mess, String time) {
        if(messagePicture != null) {
            if (drawable != null) {
                drawable.setBounds(0, 0, 50, 50);
                messagePicture.setImageDrawable(drawable);
            } else {
                messagePicture.setImageResource(R.mipmap.ic_launcher_round);
            }
        }
        if(messageName != null) {
            messageName.setText(name);
        }
        if(message != null) {
            message.setText(mess);
        }
        if(messageTime != null) {
            messageTime.setText(time);
        }
    }
}
