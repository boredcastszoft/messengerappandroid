package com.boredcast.messengerapp.Views;

import android.content.Context;
import android.util.AttributeSet;

import java.io.File;

public class MessageField extends android.support.v7.widget.AppCompatEditText {

    private File file;
    private boolean hasFile = false;

    public MessageField(Context context) {
        super(context);
    }

    public MessageField(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MessageField(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setFile(File file) {
        this.file = file;
        hasFile = true;
    }

    public void setHasFile(boolean h) {
        this.hasFile = h;
    }

    public File getFile() {
        return file;
    }

    public boolean hasFile() {
        return hasFile;
    }

}
