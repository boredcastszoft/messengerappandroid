package com.boredcast.messengerapp.Views;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.TextView;

import com.boredcast.log.CustomLogger;
import com.boredcast.log.Logging;
import com.boredcast.messengerapp.MessengerApplication;
import com.boredcast.messengerapp.R;
import com.boredcast.messengerapp.Tasks.BasicCommTask;
import com.boredcast.utils.ResponseCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class FriendRequest extends ConstraintLayout {

    private static final CustomLogger log = Logging.getLogger(FriendRequest.class);

    private TextView nameTextView;
    private Button acceptButton;
    private Button denyButton;

    private BasicCommTask commTask;

    private final WeakReference<Context> weakContext;
    private ResponseCallback responseCallback;

    public FriendRequest(Context context, ResponseCallback responseCallback) {
        super(context);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.friend_request_view, this);
        init();
    }

    public FriendRequest(Context context, AttributeSet attrs, ResponseCallback responseCallback) {
        super(context, attrs);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.friend_request_view, this);
        init();
    }

    public FriendRequest(Context context, AttributeSet attrs, int defStyleAttr, ResponseCallback responseCallback) {
        super(context, attrs, defStyleAttr);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.friend_request_view, this);
        init();
    }

    private void init() {
        nameTextView = findViewById(R.id.friend_req_view_name);
        acceptButton = findViewById(R.id.accept_button);
        denyButton = findViewById(R.id.deny_button);

        acceptButton.setOnClickListener(view -> {
            log.info("accept");
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("userFriendName", nameTextView.getText().toString());
                jsonObject.put("whoseFriend", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));
            } catch (JSONException e) {
                log.error(e);
            }

            if (commTask != null) {
                commTask.cancel(true);
            }

            JSONObject plus = new JSONObject();
            try {
                plus.put("name", nameTextView.getText().toString());
            } catch (JSONException e) {
                log.error(e);
            }
            commTask = new BasicCommTask("acceptfriend", jsonObject, weakContext.get(), responseCallback, plus);
            commTask.execute();
        });
        denyButton.setOnClickListener(view -> {
            log.info("deny");
            JSONObject jsonObject = new JSONObject();
            if (commTask != null) {
                commTask.cancel(true);
            }

            JSONObject plus = new JSONObject();
            try {
                plus.put("name", nameTextView.getText().toString());
            } catch (JSONException e) {
                log.error(e);
            }

            commTask = new BasicCommTask("denyfriend", jsonObject, weakContext.get(), responseCallback, plus);
            commTask.execute();
        });
    }

    public void setName(String name) {
        if (nameTextView != null) {
            nameTextView.setText(name);
        }
    }

    public String getName() {
        if(nameTextView != null) {
            return nameTextView.getText().toString();
        }
        return null;
    }


}
