package com.boredcast.messengerapp.Views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.boredcast.messengerapp.R;

public class FriendView extends ConstraintLayout {

    private ImageView friendImage;
    private TextView friendName;
    private boolean isHighlighted;

    public FriendView(Context context) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.friendview_layout, this);
        init();
    }

    public FriendView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.friendview_layout, this);
        init();
    }

    public FriendView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.friendview_layout, this);
        init();
    }

    private void init() {
        friendImage = findViewById(R.id.friend_image);
        friendName = findViewById(R.id.friendname);
    }

    public String getFriend() {
        return friendName.getText().toString();
    }

    public boolean isHighlighted() {
        return isHighlighted;
    }

    public void highlight(boolean visible) {
        if (visible) {
            isHighlighted = true;
            friendName.setTextColor(getResources().getColor(R.color.grey));
            setBackgroundColor(getResources().getColor(R.color.orange));
        } else {
            isHighlighted = false;
            friendName.setTextColor(getResources().getColor(R.color.orange));
            setBackgroundColor(Color.TRANSPARENT);
        }
    }

    public void setFriend(String name, Drawable drawable) {
        if (friendName != null) {
            friendName.setText(name);
        }
        if (friendImage != null) {
            if (drawable != null) {
                drawable.setBounds(0, 0, 50, 50);
                friendImage.setImageDrawable(drawable);
            } else {
                friendImage.setImageResource(R.mipmap.ic_launcher_round);
            }
        }
    }
}
