package com.boredcast.messengerapp;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import com.boredcast.log.Logging;

public class MessengerApplication extends Application {

    public static SharedPreferences config;

    @Override
    public void onCreate() {
        super.onCreate();
        Logging.init();
        config = PreferenceManager.getDefaultSharedPreferences(this);
        config.edit().putString(PreferenceKeys.SERVER_URL, "http://10.1.8.144:8181").apply();
    }

    public static final class PreferenceKeys {

        public static final String SERVER_URL = "serverurl";
        public static final String USER_ID = "userid";

    }

    public static final class ServerActions {
        public static final String MESSAGE = BuildConfig.APPLICATION_ID + ".MESSAGE";
    }

    public static final class Response {

        public static final String OK = "ok";
        public static final String NOT_OK ="no";

    }
}
