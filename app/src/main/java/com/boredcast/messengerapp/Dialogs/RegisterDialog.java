package com.boredcast.messengerapp.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.boredcast.log.CustomLogger;
import com.boredcast.log.Logging;
import com.boredcast.messengerapp.R;
import com.boredcast.messengerapp.Tasks.BasicCommTask;
import com.boredcast.utils.ResponseCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class RegisterDialog extends Dialog {

    private static final CustomLogger log = Logging.getLogger(RegisterDialog.class);

    private EditText usernameField;
    private EditText nameField;
    private EditText passwordField;

    private final WeakReference<Context> weakContext;
    private ResponseCallback responseCallback;

    private BasicCommTask registerTask;

    public RegisterDialog(@NonNull Context context, ResponseCallback responseCallback) {
        super(context);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
    }

    public RegisterDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.weakContext = new WeakReference<>(context);
    }

    protected RegisterDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.weakContext = new WeakReference<>(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.register_layout);

        Button registerButton = findViewById(R.id.register_button);
        usernameField = findViewById(R.id.username_field);
        nameField = findViewById(R.id.name_field);
        passwordField = findViewById(R.id.password_field);

        registerButton.setOnClickListener(v -> {
            String userName = usernameField.getText().toString();
            String name = nameField.getText().toString();
            String password = passwordField.getText().toString();
            log.info(userName + ":" + name + ":" + password);

            if (registerTask != null) {
                registerTask.cancel(true);
            }

            if (!userName.equals("") && !name.equals("") && !password.equals("")) {

                JSONObject json = new JSONObject();

                try {
                    json.put("userName", userName);
                    json.put("name", name);
                    json.put("password", password);
                } catch (JSONException e) {
                    log.error(e);
                }

                registerTask = new BasicCommTask("register", json, weakContext.get(), responseCallback);
                registerTask.execute();
            } else {
                Toast.makeText(weakContext.get(), "Fill every field!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
