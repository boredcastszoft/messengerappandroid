package com.boredcast.messengerapp.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Window;
import android.widget.Button;

import com.boredcast.messengerapp.MessengerApplication;
import com.boredcast.messengerapp.R;
import com.boredcast.messengerapp.Tasks.BasicCommTask;
import com.boredcast.utils.ResponseCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class LogoutDialog extends Dialog {

    private BasicCommTask logoutTask;

    private final WeakReference<Context> weakContext;
    private final ResponseCallback responseCallback;

    public LogoutDialog(@NonNull Context context, ResponseCallback responseCallback) {
        super(context);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
    }

    public LogoutDialog(@NonNull Context context, int themeResId, ResponseCallback responseCallback) {
        super(context, themeResId);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
    }

    protected LogoutDialog(@NonNull Context context, boolean cancelable, @Nullable DialogInterface.OnCancelListener cancelListener, ResponseCallback responseCallback) {
        super(context, cancelable, cancelListener);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.logout_layout);

        Button ok_button = findViewById(R.id.logout_ok_button);
        Button notok_button = findViewById(R.id.logout_notok_button);

        ok_button.setOnClickListener(view -> {
            JSONObject json = new JSONObject();
            try {
                json.put("userName", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));
                json.put("logOutTime", System.currentTimeMillis());
            } catch (JSONException ignored) {
            }

            if (logoutTask != null) {
                logoutTask.cancel(true);
            }

            logoutTask = new BasicCommTask("logout", json, weakContext.get(), responseCallback);
            logoutTask.execute();

        });

        notok_button.setOnClickListener(view -> dismiss());
    }
}
