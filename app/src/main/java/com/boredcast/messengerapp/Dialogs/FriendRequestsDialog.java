package com.boredcast.messengerapp.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.boredcast.log.CustomLogger;
import com.boredcast.log.Logging;
import com.boredcast.messengerapp.MessengerApplication;
import com.boredcast.messengerapp.R;
import com.boredcast.messengerapp.Tasks.GetDataTask;
import com.boredcast.messengerapp.Views.FriendRequest;
import com.boredcast.utils.ResponseCallback;
import com.boredcast.utils.ToActivitiyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class FriendRequestsDialog extends Dialog implements ResponseCallback {

    private static final CustomLogger log = Logging.getLogger(FriendRequestsDialog.class);

    private final WeakReference<Context> weakContext;

    private ScrollView scrollView;
    private LinearLayout reqLayout;
    private ProgressBar progressBar;

    private ToActivitiyCallback toActivitiyCallback;

    private GetDataTask getFriendRequests;

    private List<String> friendReqList;
    private Map<Integer, FriendRequest> requests;

    public FriendRequestsDialog(@NonNull Context context, ResponseCallback responseCallback, ToActivitiyCallback toActivitiyCallback) {
        super(context);
        this.weakContext = new WeakReference<>(context);
        this.toActivitiyCallback = toActivitiyCallback;
    }

    public FriendRequestsDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.weakContext = new WeakReference<>(context);
    }

    protected FriendRequestsDialog(@NonNull Context context, boolean cancelable, @Nullable DialogInterface.OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.weakContext = new WeakReference<>(context);
    }

    private void callActivitiy(String kind, ToActivitiyCallback toActivitiyCallback) {
        toActivitiyCallback.onCall(kind);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.friend_requests_layout);

        scrollView = findViewById(R.id.friend_req_holder);
        reqLayout = findViewById(R.id.friend_req_layout);
        findViewById(R.id.no_req_label).setVisibility(View.GONE);
        progressBar = findViewById(R.id.progressbar_req);
        progressBar.setVisibility(View.GONE);

        findViewById(R.id.friend_req_back_button).setOnClickListener(view -> {
            callActivitiy("friendrequest", toActivitiyCallback);
            dismiss();
        });

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userName", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));
        } catch (JSONException e) {
            log.error(e);
        }

        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }

        if (getFriendRequests != null) {
            getFriendRequests.cancel(true);
        }
        getFriendRequests = new GetDataTask("getfriendrequest", jsonObject, weakContext.get(), this);
        getFriendRequests.execute();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (getFriendRequests != null) {
            getFriendRequests.cancel(true);
        }
    }

    @Override
    public void onResponse(String kind, JSONObject json) {
        if (kind != null) {
            switch (kind) {
                case "getfriendrequest":
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }
                    friendReqList = new ArrayList<>();
                    requests = new TreeMap<>();
                    try {
                        JSONArray arr = json.getJSONArray("data");
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject tmp = arr.getJSONObject(i);
                            friendReqList.add(tmp.getString("sender"));
                        }
                    } catch (JSONException e) {
                        log.error(e);
                    }
                    int i = 1;
                    if (!friendReqList.isEmpty()) {
                        for (String s : friendReqList) {
                            log.info(s);
                            FriendRequest fr = new FriendRequest(weakContext.get(), this);
                            fr.setName(s);
                            fr.setId(i);
                            reqLayout.addView(fr);
                            requests.put(i, fr);
                            scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));
                            i++;
                        }
                    } else {
                        findViewById(R.id.no_req_label).setVisibility(View.VISIBLE);
                    }
                    break;
                case "acceptfriend":
                    try {
                        String state = json.getString("state");
                        switch (state) {
                            case MessengerApplication.Response.OK:
                                Toast.makeText(weakContext.get(), "Friend request accepted!", Toast.LENGTH_SHORT).show();
                                JSONObject tmp = json.getJSONObject("plus");
                                if (tmp != null) {
                                    String name = tmp.getString("name");
                                    if (name != null) {
                                        friendReqList.remove(name);
                                        for(Map.Entry<Integer, FriendRequest> s: requests.entrySet()) {
                                            if(s.getValue().getName().equals(name)) {
                                                s.getValue().setVisibility(View.GONE);
                                            }
                                        }
                                    }
                                    if (friendReqList.isEmpty()) {
                                        findViewById(R.id.no_req_label).setVisibility(View.VISIBLE);
                                    }
                                }
                                break;
                            case MessengerApplication.Response.NOT_OK:
                                Toast.makeText(weakContext.get(), "There was an Error!", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    } catch (JSONException e) {
                        log.error(e);
                    }
                    break;
                case "denyfriend":
                    try {
                        String state = json.getString("state");
                        switch (state) {
                            case MessengerApplication.Response.OK:
                                Toast.makeText(weakContext.get(), "Friend request denied!", Toast.LENGTH_SHORT).show();
                                JSONObject tmp = json.getJSONObject("plus");
                                if (tmp != null) {
                                    String name = tmp.getString("name");
                                    if (name != null) {
                                        friendReqList.remove(name);
                                        for(Map.Entry<Integer, FriendRequest> s: requests.entrySet()) {
                                            if(s.getValue().getName().equals(name)) {
                                                s.getValue().setVisibility(View.GONE);
                                            }
                                        }
                                    }
                                    if (friendReqList.isEmpty()) {
                                        findViewById(R.id.no_req_label).setVisibility(View.VISIBLE);
                                    }
                                }
                                break;
                            case MessengerApplication.Response.NOT_OK:
                                Toast.makeText(weakContext.get(), "There was an Error!", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    } catch (JSONException e) {
                        log.error(e);
                    }
            }
        }
    }
}
