package com.boredcast.messengerapp.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.boredcast.log.CustomLogger;
import com.boredcast.log.Logging;
import com.boredcast.messengerapp.R;
import com.boredcast.messengerapp.Tasks.CreateGroup;
import com.boredcast.utils.ResponseCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class CreateGroupDialog extends Dialog {

    private static final CustomLogger log = Logging.getLogger(CreateGroupDialog.class);

    private EditText groupNameField;
    private EditText usersField;

    private final WeakReference<Context> weakContext;
    private ResponseCallback responseCallback;

    private CreateGroup createGroup;

    public CreateGroupDialog(@NonNull Context context, ResponseCallback responseCallback) {
        super(context);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
    }

    public CreateGroupDialog(@NonNull Context context, int themeResId, ResponseCallback responseCallback) {
        super(context, themeResId);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
    }

    protected CreateGroupDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener, ResponseCallback responseCallback) {
        super(context, cancelable, cancelListener);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.create_group_layout);

        groupNameField = findViewById(R.id.groupname_field);
        usersField = findViewById(R.id.people_in_group_field);

        Button create = findViewById(R.id.create_button);
        create.setOnClickListener(view -> {
            String groupName = groupNameField.getText().toString();
            String[] users = usersField.getText().toString().split(",");
            /*List<String> us = Arrays.asList(users);
            us.add(MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));*/

            JSONArray arr = new JSONArray();
            for (String user : users) {
                JSONObject json = new JSONObject();
                try {
                    json.put("userName", user);
                } catch (JSONException e) {
                    log.error(e);
                }
                arr.put(json);
            }

            JSONObject json = new JSONObject();
            try {
                json.put("groupName", groupName);
                json.put("users", arr.toString());
            } catch (JSONException e) {
                log.error(e);
            }

            if (createGroup != null) {
                createGroup.cancel(true);
            }

            createGroup = new CreateGroup(json, weakContext.get(), responseCallback);
            createGroup.execute();

        });
    }
}
