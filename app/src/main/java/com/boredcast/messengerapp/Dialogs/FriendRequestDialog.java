package com.boredcast.messengerapp.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.boredcast.log.CustomLogger;
import com.boredcast.log.Logging;
import com.boredcast.messengerapp.MessengerApplication;
import com.boredcast.messengerapp.R;
import com.boredcast.messengerapp.Tasks.BasicCommTask;
import com.boredcast.utils.ResponseCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class FriendRequestDialog extends Dialog {

    private static final CustomLogger log = Logging.getLogger(FriendRequestDialog.class);

    private EditText friendName;

    private BasicCommTask friendRequest;

    private final WeakReference<Context> weakContext;
    private ResponseCallback responseCallback;

    public FriendRequestDialog(@NonNull Context context, ResponseCallback responseCallback) {
        super(context);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
    }

    public FriendRequestDialog(@NonNull Context context, int themeResId, ResponseCallback responseCallback) {
        super(context, themeResId);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
    }

    protected FriendRequestDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener, ResponseCallback responseCallback) {
        super(context, cancelable, cancelListener);
        this.weakContext = new WeakReference<>(context);
        this.responseCallback = responseCallback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.friend_request_layout);

        Button sendReq = findViewById(R.id.request_button);
        friendName = findViewById(R.id.friendname_field);

        sendReq.setOnClickListener(v -> {
            String friendNam = friendName.getText().toString();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("userFriendName", friendNam);
                jsonObject.put("whoseFriend", MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.USER_ID, null));
            } catch (JSONException e) {
                log.error(e);
            }

            if (friendRequest != null) {
                friendRequest.cancel(true);
            }
            friendRequest = new BasicCommTask("friendrequest", jsonObject, weakContext.get(), responseCallback);
            friendRequest.execute();
        });

    }
}
