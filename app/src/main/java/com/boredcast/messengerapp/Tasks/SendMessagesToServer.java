package com.boredcast.messengerapp.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.boredcast.messengerapp.MessengerApplication;
import com.boredcast.utils.Tools;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class SendMessagesToServer extends AsyncTask<Void, Void, JSONObject> {

    private JSONObject json;
    private final WeakReference<Context> weakContext;

    public SendMessagesToServer(JSONObject json, Context weakContext) {
        this.weakContext = new WeakReference<>(weakContext);
        this.json = json;
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();

            okhttp3.RequestBody body = RequestBody.create(JSON, json.toString());
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.SERVER_URL, null) + "/message")
                    .post(body)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                return Tools.parseJSONStringToJSONObject(networkResp);
            }

        } catch (Exception ex) {
            String error = String.format("{\"result\":\"false\",\"error\":\"%s\"}", ex.getMessage());
            return Tools.parseJSONStringToJSONObject(error);
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject res) {
        Context context = weakContext.get();
        if (res == null) {
            Toast.makeText(context, "Could not send Message", Toast.LENGTH_SHORT).show();
        }
    }

}
