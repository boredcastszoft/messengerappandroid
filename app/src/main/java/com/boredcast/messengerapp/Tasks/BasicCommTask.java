package com.boredcast.messengerapp.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.boredcast.log.CustomLogger;
import com.boredcast.log.Logging;
import com.boredcast.messengerapp.MessengerApplication;
import com.boredcast.utils.ResponseCallback;
import com.boredcast.utils.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class BasicCommTask extends AsyncTask<Void, Void, JSONObject> {

    private static final CustomLogger log = Logging.getLogger(BasicCommTask.class);

    private String kind;
    private JSONObject json;
    private JSONObject plus;
    private final WeakReference<Context> weakContext;
    private ResponseCallback responseCallback;
    private boolean err = false;

    public BasicCommTask(String kind, JSONObject json, Context weakContext, ResponseCallback responseCallback, JSONObject plus) {
        this.kind = kind;
        this.json = json;
        this.weakContext = new WeakReference<>(weakContext);
        this.responseCallback = responseCallback;
        this.plus = plus;
    }

    public BasicCommTask(String kind, JSONObject json, Context weakContext, ResponseCallback responseCallback) {
        this.kind = kind;
        this.json = json;
        this.weakContext = new WeakReference<>(weakContext);
        this.responseCallback = responseCallback;
    }

    private void callResp(ResponseCallback responseCallback, JSONObject json) {
        responseCallback.onResponse(kind, json);
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {

        JSONObject res = null;

        try {

            log.info("jézi: " + json.toString());

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();

            okhttp3.RequestBody body = RequestBody.create(JSON, json.toString());
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.SERVER_URL, null) + "/" + kind)
                    .post(body)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                log.info(networkResp);
                res = Tools.parseJSONStringToJSONObject(networkResp);
                if (plus != null) {
                    res.put("plus", plus);
                }
                log.info(res.toString());
                try {
                    int error = res.getInt("status");
                    if (error == 500) {
                        err = true;
                    }
                } catch (JSONException ignored) {
                }
            }
        } catch (Exception ex) {
            String er = String.format("{\"result\":\"false\",\"error\":\"%s\"}", ex.getMessage());
            res = Tools.parseJSONStringToJSONObject(er);
            err = true;
        }

        return res;
    }

    @Override
    protected void onPostExecute(JSONObject res) {
        Context context = weakContext.get();
        if (err) {
            Toast.makeText(context, "There was an error!", Toast.LENGTH_SHORT).show();
            callResp(responseCallback, res);
        } else if (res != null) {
            callResp(responseCallback, res);
        } else {
            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
        }
    }
}
