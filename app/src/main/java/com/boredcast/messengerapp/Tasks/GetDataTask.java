package com.boredcast.messengerapp.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.boredcast.log.CustomLogger;
import com.boredcast.log.Logging;
import com.boredcast.messengerapp.MessengerApplication;
import com.boredcast.utils.ResponseCallback;
import com.boredcast.utils.Tools;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class GetDataTask extends AsyncTask<Void, Void, JSONObject> {

    private static final CustomLogger log = Logging.getLogger(GetDataTask.class);

    private String kind;
    private JSONObject json;
    private final WeakReference<Context> weakContext;
    private ResponseCallback responseCallback;

    public GetDataTask(String kind, JSONObject json, Context weakContext, ResponseCallback responseCallback) {
        this.kind = kind;
        this.json = json;
        this.weakContext = new WeakReference<>(weakContext);
        this.responseCallback = responseCallback;
    }

    private void callResp(ResponseCallback responseCallback, JSONObject json) {
        responseCallback.onResponse(kind, json);
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();

            okhttp3.RequestBody body = RequestBody.create(JSON, json.toString());
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(MessengerApplication.config.getString(MessengerApplication.PreferenceKeys.SERVER_URL, null) + "/" + kind)
                    .post(body)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                networkResp = "{ \"data\" : " + networkResp + "}";
                log.info("networkResp: " + networkResp);
                return Tools.parseJSONStringToJSONObject(networkResp);
            }
        } catch (Exception ex) {
            String error = String.format("{\"result\":\"false\",\"error\":\"%s\"}", ex.getMessage());
            return Tools.parseJSONStringToJSONObject(error);
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject res) {
        Context context = weakContext.get();
        if (res == null) {
            Toast.makeText(context, "There was an error!", Toast.LENGTH_SHORT).show();
        } else {
            callResp(responseCallback, res);
        }
    }
}
