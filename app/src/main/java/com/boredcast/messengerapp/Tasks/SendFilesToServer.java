package com.boredcast.messengerapp.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.boredcast.utils.Tools;

import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SendFilesToServer extends AsyncTask<Void, Void, JSONObject> {

    private File file;
    private final WeakReference<Context> weakContext;

    public SendFilesToServer(File file, Context weakContext) {
        this.file = file;
        this.weakContext = new WeakReference<>(weakContext);
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        try {

            final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");

            RequestBody req = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("file", file.getName(), RequestBody.create(MEDIA_TYPE_JPG, file)).build();

            Request request = new Request.Builder()
                    .url("url")
                    .post(req)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                return Tools.parseJSONStringToJSONObject(networkResp);
            }
        } catch (Exception ex) {
            String error = String.format("{\"result\":\"false\",\"error\":\"%s\"}", ex.getMessage());
            return Tools.parseJSONStringToJSONObject(error);
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject res) {
        Context context = weakContext.get();
        if (res == null) {
            Toast.makeText(context, "Could not send Message", Toast.LENGTH_SHORT).show();
        }
    }
}
