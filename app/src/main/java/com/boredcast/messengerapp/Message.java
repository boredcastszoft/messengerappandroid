package com.boredcast.messengerapp;

public class Message {

    private String sender;
    private String receiver;
    private String message;
    private long sentTime;


    public Message(String sender, String receiver, String message, long sentTime) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
        this.sentTime = sentTime;
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getMessage() {
        return message;
    }

    public long getSentTime() {
        return sentTime;
    }
}
