package com.boredcast.log;

public interface LogListener {
    void onLogMessage(final String logMessage);
}
