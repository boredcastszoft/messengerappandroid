package com.boredcast.utils;

import org.json.JSONObject;

public interface ResponseCallback {
    void onResponse(String kind, JSONObject json);
}
